#!/bin/python3

import datetime

class Date:
    def __init__(self, config):
        self.config = config
        self.criticity = None

    def on_click(self, event: dict):
        pass

    def return_value(self) -> tuple:
        x = datetime.datetime.now()
        date_str = x.strftime("%a") + ' '
        date_str += x.strftime("%d") + ' '
        date_str += x.strftime("%B") + ' '
        date_str += x.strftime("%H") + ':'
        date_str += x.strftime("%M") + ':'
        date_str += x.strftime("%S")
        return (self.criticity, date_str)
