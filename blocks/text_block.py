#!/bin/python3

class Text_block:
    def __init__(self, config):
        self.config = config
        self.text = config['text']
        self.criticity = None

    def on_click(self, event: str):
        pass

    def return_value(self) -> tuple:
        return (self.criticity, self.text)