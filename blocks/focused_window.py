#!/bin/python3

import subprocess
import time
import util

class Focused_window:
    def __init__(self, config):
        self.config = config
        self.to_roll = {}

    def rolling_text(self, to_print: str) -> str:
        now = time.time()
        if to_print in self.to_roll:
            if time.time() - self.to_roll[to_print][0] >= self.config['roll_interval']:
                if len(to_print) - self.to_roll[to_print][1] > self.config['max_width']:
                    self.to_roll[to_print][1] += 1
                else:
                    self.to_roll[to_print][1] = 0
                self.to_roll[to_print][0] = now
            return(to_print[self.to_roll[to_print][1]:
                            self.to_roll[to_print][1]+self.config['max_width']])
        else:
            self.to_roll.update({to_print:[now, 0]})
            return(to_print[0:self.config['max_width']])
        pass

    def get_id(self) -> str:
        root_prop = subprocess.Popen("xprop -root | grep _ACTIVE_WINDOW\\(",
                             shell=True, stdout=subprocess.PIPE).stdout.read().strip()
        root_prop = root_prop.decode('utf-8').split(' ')
        prop_list = [x.replace('\n', '') for x in root_prop]
        return prop_list[-1]

    def get_name(self, id: str) -> str:
        #xprop -id 0xa0000a
        root_prop = subprocess.Popen("xprop -id " + id + " | grep _NET_WM_NAME\\(",
                             shell=True, stdout=subprocess.PIPE).stdout.read().strip()
        root_prop = root_prop.decode('utf-8').split(' ')
        prop_list = [x.replace('\n', '') for x in root_prop]
        return ' '.join(prop_list[2:])[1:-1]

    def on_click(self, event: str):
        pass

    def return_value(self) -> tuple:
        to_send = self.get_name(self.get_id())
        if to_send in self.to_roll:
            to_print, last_pos, tick = util.roll_text(self.to_roll[to_send])
            to_roll = util.RollingText(to_send, last_pos, self.config['max_width'], tick, self.config['roll_interval'])
        else:
            to_roll = util.RollingText(to_send, 0, self.config['max_width'], 0, self.config['roll_interval'])
            to_print, last_pos, tick = util.roll_text(to_roll)
        self.to_roll.update({to_send:to_roll})
        return (None, to_print)