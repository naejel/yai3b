#!/bin/python3

import util

class Battery:
    def __init__(self, config: dict):
        self.config = config
        self.path = config['path']
        self.output_format = config['output_format']
        self.capacity = None
        self.status = None
        self.remaining = None
        self.criticity = None
        self.error = None

    def read_battery(self):
        try:
            if self.path == 'acpi':
                res = util.get_command('acpi', ',')
                self.status = res[0].split(':')[1][1:]
                self.capacity = int(res[1][1:-1])
                if len(res) > 2:
                    self.remaining = res[2].split(' ')[1]
            else:
                self.capacity = int(util.get_command('cat ' + self.path +'capacity')[0])
                self.status = util.get_command('cat ' + self.path +'status')[0]
                if self.status != 'Full':
                    charge_full = int(util.get_command('cat ' + self.path +'charge_full')[0])
                    charge_now = int(util.get_command('cat ' + self.path +'charge_now')[0])
                    current_now = int(util.get_command('cat ' + self.path +'current_now')[0])
                    remaining_seconds = int(((charge_full - charge_now) / current_now) * 3600)
                    self.remaining = util.second_to_str(remaining_seconds,
                                                        "%d:%02d:%02d")
                else:
                    self.remaining = None
        except:
            self.error = 'Cannot read: ' + self.path

    def on_click(self, event: dict):
        pass

    def return_value(self) -> tuple:
        to_return = ''
        self.read_battery()
        if self.error:
            return('error', self.error)
        if self.status != 'Full':
            self.criticity = "warning"
        else:
            self.criticity = "idle"
        if self.capacity <= 25:
            self.criticity = 'critical'
        if self.output_format == 'icon':
            if self.status == ' Charging':
                to_return += "\uF1E6" + ' '
            elif self.status == ' Discharging':
                to_return += "\uF0E7" + ' '
            if self.capacity == 100:
                to_return += "\uF240"
            elif self.capacity <= 10:
                to_return += "\uF244"
            elif self.capacity <= 25:
                to_return += "\uF243"
            elif self.capacity <= 50:
                to_return += "\uF242"
            elif self.capacity <= 99:
                to_return += "\uF241"
        if self.output_format != "icon" or self.capacity < 100:
            to_return += ' ' + str(self.capacity) + '%'
        if self.status != 'Full':
            if self.remaining:
                to_return += ': ' + self.remaining[:-3]
        return (self.criticity, to_return)
