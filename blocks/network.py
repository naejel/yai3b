#!/bin/python3

import shlex
import subprocess
import util

class Network:
    def __init__(self, config: str):
        self.config = config
        self.ip = None
        self.connection = None

    def get_nmcli(self, interface: str):
        res = util.get_command('nmcli device show ' + interface)
        for entry in res:
            if 'IP4.ADDRESS' in entry:
                self.ip = entry.split(' ')[-1]
            elif 'GENERAL.CONNECTION' in entry:
                self.connection = entry.split(' ')[-1]

    def on_click(self, event: dict):
        pass

    def return_value(self) -> tuple:
        to_send = ''
        if 'icon' in self.config:
            to_send += self.config['icon']
        if self.config['driver'] == 'nmcli':
            self.get_nmcli(self.config['interface'])
            for info in self.config['infos']:
                val = getattr(self, info)
                to_send += str(val)
                to_send += self.config['sep_block']
        return(None, to_send)