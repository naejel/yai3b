#!/bin/python3

import util

class Disk_usage:
    def __init__(self, config: str):
        self.config = config
        self.to_send = ''
        self.last_tick = 0

    def on_click(self, event: dict):
        pass

    def return_value(self) -> tuple:
        ret, tick = util.interval_is_elapsed((self.last_tick, self.config['time_interval']))
        self.last_tick = tick
        if ret is True:
            self.to_send = ''
            if 'icon' in self.config:
                self.to_send += self.config['icon']
            res = list(filter(None, util.get_command('df -h ' +  self.config['path'])[1].split(' ')))
            for info in self.config['infos']:
                if info == 'sys':
                    self.to_send += res[0]
                if info == 'size':
                    self.to_send += res[1]
                if info == 'used':
                    self.to_send += res[2]
                if info == 'left':
                    self.to_send += res[3]
                if info =='percent':
                    self.to_send += res[4]
                if info != self.config['infos'][-1]:
                    self.to_send += self.config['sep_block']
        return(None, self.to_send)