#!/bin/python3

import time
import shlex, subprocess
from typing import NamedTuple
from collections import namedtuple


class RollingText(NamedTuple):
    to_print: str
    last_pos: int
    max_width: int
    last_tick: float
    interval: float

def second_to_str(to_convert: float, format: str) -> str:
    min, sec = divmod(to_convert, 60)
    hour, min = divmod(min, 60)
    return format % (hour, min, sec)

def now() -> float:
    return time.time()

def interval_is_elapsed(to_calc: tuple()) -> tuple():
    """Respond if interval time is elapsed since last tick

    Arguments:
        to_calc::tuple(2)
            last_tick::float
            interval::float
    Returns:
        return::tuple(2)
            result::bool
            now::float
    """
    now = time.time()
    if now - to_calc[0] >= to_calc[1]:
        return (True, now)
    else:
        return (False, to_calc[0])

def roll_text(to_roll: RollingText) -> tuple():
    """Return schrunk of text

    Arguments:
        to_roll::tuple(5)
            to_print::str
            last_pos::int
            max_width::int
            last_tick::float
            interval::float

    Returns:
        return::tuple(3)
            to_print::str
            last_pos::int
            tick::float
    """
    to_print, last_pos, max_width, last_tick, interval = to_roll
    res, tick = interval_is_elapsed((last_tick, interval))
    if res is True:
        if len(to_print) - last_pos > max_width:
            last_pos += 1
        else:
            last_pos = 0
    to_print = to_print[last_pos:last_pos+max_width]
    return(to_print, last_pos, tick)

def get_command(cmd: str, to_split: str='\n', shell: bool=False) -> list:
    try:
        if shell is False:
            res = subprocess.Popen(shlex.split(cmd),
                                    stdout=subprocess.PIPE, shell=shell).stdout\
                                    .read().strip().decode('utf-8').split(to_split)
        else:
            res = subprocess.Popen(cmd,
                             shell=True, stdout=subprocess.PIPE).stdout.read().strip()\
                                                                .decode('utf-8').split(to_split)
        return list(filter(None, [x.replace('\n', '') for x in res]))
    except:
        return None

def launch_command(cmd: str) -> bool:
    try:
        subprocess.Popen(shlex.split(cmd), stdout=subprocess.DEVNULL)
        return True
    except:
        return False