#!/bin/python3

import util

class Audio:
    def __init__(self, config):
        self.config = config
        self.step = config['step']
        self.driver = config['driver']
        self.device_name = config['device_name']
        self.volume = None
        self.muted = None
        self.criticity = None
        self.error = None

    def get_infos(self):
        if self.driver == 'alsa':
            res = util.get_command('amixer get ' + self.device_name)
            if res:
                for line in res:
                    if 'Front Left:' in line:
                        self.volume = int(line.split(' ')[6][1:-2])
                        if '[on]' in line:
                            self.muted = False
                        else:
                            self.muted = True
            else:
                self.error = "Cannot get alsa"
        elif self.driver == 'pulseaudio':
            res = util.get_command('pacmd list-sinks')
            if res:
                activeCard = False
                for line in res:
                    if 'index' in line:
                        if '*' in line:
                            activeCard = True
                    if 'front-left:' in line:
                        for item in line.split(' '):
                            if '%' in item:
                                self.volume = int(item[:-1])
                    if 'muted:' in line:
                        if 'no' in line:
                            self.muted = False
                        else:
                            self.muted = True
                        if activeCard:
                            break
            else:
                self.error = "Cannot get pulseaudio"

    def up_volume(self):
        if self.driver == 'alsa':
            util.launch_command('amixer set ' + self.device_name + ' ' +
                                str(int(self.volume) + self.step) + '%')
        elif self.driver == 'pulseaudio':
            util.launch_command('pactl set-sink-volume ' +
                                self.device_name + ' +' +
                                str(self.step) + '%')

    def down_volume(self):
        if self.driver == 'alsa':
            util.launch_command('amixer set ' + self.device_name + ' ' +
                                str(int(self.volume) - self.step) + '%')
        elif self.driver == 'pulseaudio':
            util.launch_command('pactl set-sink-volume ' +
                                self.device_name + ' -' +
                                str(self.step) + '%')

    def toggle(self):
        if self.driver == 'alsa':
            util.launch_command('amixer set ' +
                                self.device_name + ' toggle')
        elif self.driver == 'pulseaudio':
            util.launch_command('pactl set-sink-mute  ' +
                                self.device_name + ' toggle')

    def on_click(self, event: dict):
        if event['button'] == 3:
            self.toggle()
        elif event['button'] == 4:
            self.up_volume()
        elif event['button'] == 5:
            self.down_volume()

    def return_value(self) -> tuple:
        to_send = ' '
        self.get_infos()
        if self.volume:
            if self.volume >= 75:
                self.criticity = 'critical'
            elif self.volume >= 50:
                self.criticity = "warning"
            else:
                self.criticity = "idle"
            if self.config['output_format'] == 'icon':
                if self.muted is True:
                    to_send += '\uF026'
                elif self.volume <= 25:
                    to_send += '\uF026'
                elif self.volume <= 50:
                    to_send += '\uF027'
                else:
                    to_send += '\uF028'
            if self.muted is True:
                to_send += ' ' + '\uF00D' + ' '
            else:
                to_send += ' ' + str(self.volume) + '% '
        return (self.criticity, to_send)
