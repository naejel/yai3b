#!/bin/python3

import util
import os

class Cpu_usage:
    def __init__(self, config: str):
        self.time_interval = config['time_interval']
        self.icon = config['icon']
        self.last_tick = 0
        self.to_send = ''


    def on_click(self, event: dict):
        self.last_tick = 0

    def return_value(self) -> tuple:
        ret, tick = util.interval_is_elapsed((self.last_tick, self.time_interval))
        self.last_tick = tick
        if ret is True:
            usage = [x / os.cpu_count() * 100 for x in os.getloadavg()][0]
            self.to_send = self.icon
            self.to_send += str(round(usage,1)) + '%'
        return(None, self.to_send)