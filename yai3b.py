#!/bin/python3
"""i3/Sway status bar in Python
"""

import shlex, subprocess
import sys
import os
import json
import importlib
import select
import argparse

__author__ = "Jean LE QUELLEC"
__copyright__ = "Copyright 2020, Yet Another i3 Bar"
__credits__ = ["Jean LE QUELLEC"]
__license__ = "MIT"
__version__ = "0.2.0"
__maintainer__ = "Jean LE QUELLEC"
__email__ = "Contact via Gitlab"
__status__ = "Production"

# Make blocks directory importable
cwd = os.path.dirname(__file__)
sys.path.insert(0, cwd + '/blocks/')

import util

class Logger:
    def __init__(self, filename: str):
        import logging
        from logging.handlers import RotatingFileHandler
        self.logger = logging.getLogger('yai3b_logger')
        self.logger.setLevel(logging.DEBUG)
        self.handler = RotatingFileHandler(filename, maxBytes=200000, backupCount=1)
        self.logger.addHandler(self.handler)


class Theme:
    def __init__(self, theme: dict):
        self.colors = theme

    def get_xcolors(self) -> bool:
        x = subprocess.run(['xrdb', '-query'], stdout=subprocess.PIPE)
        lines = x.stdout.decode().split('\n')
        for line in filter(lambda l : l.startswith('*'), lines):
            prop, _, value = line.partition(':\t')
            self.colors[prop[1:]] = value


class Block:
    def __init__(self, name: str, text: str, background: str, foreground: str):
        self.name = name
        self.text = text
        self.background = background
        self.foreground = foreground
        self.dict = {}

    def update_dict(self):
        self.dict = { "name": self.name, "full_text": self.text,
                      "color": self.foreground,
                      "background": self.background, "separator": False,
                      "separator_block_width": 0}

    def get_dict(self) -> dict:
        self.update_dict()
        return self.dict

class i3Bar:
    def __init__(self, log_file: str):
        self.logger = None
        if log_file:
            self.logger = Logger(log_file)
            self.logger.logger.debug('Yai3b start')
        self.header = {"version": 1, "click_events": True}
        print(json.dumps(self.header))
        print('[')
        print('[]')

    def send_to_bar(self, content: str):
        print(',')
        print(json.dumps(content))
        if self.logger:
            self.logger.logger.debug(content)

    def send_error(self, content: str):
        to_send = ' '
        to_send = to_send.join(content.split('\n'))
        to_send = [{"name": 'Error', "full_text": to_send, "color": "#FF0000",
                    "markup":"pango", "background": "#000000", "separator": False,
                    "separator_block_width": 0}, {"full_text": ' '}]
        if self.logger:
            self.logger.logger.error(content)
        while 1:
            print(',')
            print(json.dumps(to_send))

    def get_event(self) -> str:
        if select.select([sys.stdin],[],[],0)[0]:
            return sys.stdin.readline()
        else:
            return None

class Blocks:
    def __init__(self, blocks: dict):
        self.blocks = blocks
        self.blocks_instance = {}

    def import_blocks(self) -> str:
        for block_name, config in self.blocks.items():
            block_number = None
            try:
                block_number = str(int(block_name[-1]))
                block_name = block_name[:-1]
            except:
                pass
            try:
                module = importlib.import_module(block_name)
                class_ = getattr(module, block_name.capitalize())
                if block_number:
                    self.blocks_instance.update({block_name+block_number: class_(config)})
                else:
                    self.blocks_instance.update({block_name: class_(config)})
            except:
                return block_name

class Yai3psb:
    def __init__(self, config_file: str, xrdb: bool, no_over: bool, log_file: str):
        self.config_file = config_file
        self.xrdb = xrdb
        self.no_over = no_over
        self.loop = True
        self.to_send = []
        self.is_first = True
        self.prev_background = ''
        self.bar = i3Bar(log_file)
        self.logger = None
        if log_file:
            self.logger = Logger(log_file)
        try:
            with open(self.config_file, 'r') as f:
                self.config = json.load(f)
        except:
            self.bar.send_error('Cannot Read: ' +  self.config_file)
        if 'theme' in self.config:
            self.theme = Theme(self.config['theme'])
        elif self.xrdb is False:
            self.bar.send_error('No Theme in config')
        else:
            self.theme = Theme({})
        if 'blocks' in self.config:
            self.blocks = Blocks(self.config['blocks'])
        else:
            self.bar.send_error('No blocks in config')
        ret = self.blocks.import_blocks()
        if ret:
            self.bar.send_error('Cannot import block: ' + str(ret) + " from config file")

    def read_event(self):
        event = self.bar.get_event()
        is_event = False
        if event:
            if event[0] == ',':
                d_event = eval(event[1:])
                is_event = True
        if is_event is True:
            if d_event['name'] in self.blocks.blocks:
                self.launch_event(d_event)
            elif '.' in d_event['name']:
                if d_event['name'].split('.')[0] in self.blocks.blocks:
                    self.launch_event(d_event)

    def launch_event(self, event: dict):
        block = self.blocks.blocks
        name = event['name']
        multiblock = False
        if '.' in name:
            multiblock = True
            name, sub_block = name.split('.')
            to_call = 'on_click_' + sub_block
        button = str(event['button'])
        custom_click = 'custom_click' + button
        if custom_click in block[name]:
            cmd = block[name][custom_click]
            try:
                util.launch_command(cmd)
            except:
                self.bar.send_error('Cannot launch custom command: ' + cmd)
            if self.no_over is False:
                if multiblock is False:
                    try:
                        self.blocks.blocks_instance[name].on_click(event)
                    except:
                        self.bar.send_error('Cannot launch on_click: ' + block[name])
                else:
                    try:
                        getattr(self.blocks.blocks_instance[name], to_call)(event)
                    except:
                        pass
        else:
            if multiblock is False:
                try:
                    self.blocks.blocks_instance[name].on_click(event)
                except:
                    self.bar.send_error('Cannot launch on_click: ' + block[name])
            else:
                try:
                    getattr(self.blocks.blocks_instance[name], to_call)(event)
                except:
                    pass

    def read_block(self, block: str) -> str:
        name = block
        multiblock = False
        self.background = None
        self.foreground = None
        if '.' in name:
            multiblock = True
            name, sub_block = name.split('.')
        block = self.blocks.blocks[name]
        if multiblock is True:
            to_call = 'get_' + sub_block
            to_call = getattr(self.blocks.blocks_instance[name], to_call)
            criticity, self.read_val = to_call()
        else:
            criticity, self.read_val = self.blocks.blocks_instance[name].return_value()
        if criticity:
            if criticity == 'error':
                self.bar.send_error('Error: '+ str(self.read_val) + ' received from Block: ' + name)
            if 'background' in block:
                self.background =  self.theme.colors[block['background']]
            if 'foreground' in block:
                self.foreground = self.theme.colors[block['foreground']]
            try:
                if self.background is None:
                    self.background = self.theme.colors[block[criticity + '_background']]
                if self.foreground is None:
                    self.foreground = self.theme.colors[block[criticity + '_foreground']]
            except:
                self.bar.send_error('Unknow Criticity: '+ str(criticity) + ' received from Block: ' + name)
        else:
            self.background =  self.theme.colors[block['background']]
            self.foreground = self.theme.colors[block['foreground']]
        self.separator = block['separator']

    def append_block(self, block: str, is_multi: bool=False):
        if self.read_val:
            if is_multi is False:
                val = ' ' + str(self.read_val) + ' '
            else:
                val = str(self.read_val)
        else:
            val = ''
        block_ = Block(block, val, self.background, self.foreground)
        if self.is_first:
            sep_block = Block('separator', self.separator, self.theme.colors['background'], self.background)
            self.is_first = False
        else:
            sep_block = Block('separator', self.separator, self.prev_background, self.background)
        self.to_send.append(sep_block.get_dict())
        self.to_send.append(block_.get_dict())
        self.prev_background = self.background

    def start_loop(self):
        while self.loop:
            if self.xrdb is True:
                self.theme.get_xcolors()
            self.read_event()
            self.to_send = []
            self.is_first = True
            self.prev_background = ''
            for block in self.blocks.blocks:
                if 'multiblocks' in self.blocks.blocks[block]:
                    for item in self.blocks.blocks[block]['multiblocks']:
                        self.read_block(str(block) + '.' +item)
                        self.append_block(str(block) + '.' +item, True)
                else:
                    self.read_block(block)
                    self.append_block(block)
            if 'last_separator' in self.theme.colors:
                sep_block = Block('separator', self.theme.colors['last_separator'], self.prev_background, self.theme.colors['background'])
                self.to_send.append(sep_block.get_dict())
            self.bar.send_to_bar(self.to_send)

    def stop_loop(self):
        self.loop = False

    def pause_loop(self):
        while self.loop:
            pass

def main():

    # read command args first
    parser = argparse.ArgumentParser(description="Yet Another i3 Python Status Bar")

    parser.add_argument('-c', '--config', required=True,
                        help="Path to yai3psb json config file")

    parser.add_argument('-x', '--xrdb', required=False, action='store_true',
                        help="Dynamically use colors returned by xrdb")

    parser.add_argument('-n', '--no_override', required=False, action='store_true',
                        help="Don't send custom click to blocks")

    parser.add_argument('-l', '--log', required=False,
                        help="Path to log file")


    # Parse Args
    args = parser.parse_args()

    # Instance needed class
    yai_bar = Yai3psb(args.config, args.xrdb, args.no_override, args.log)
    if args.log:
        logger = Logger(args.log)
    try:
        yai_bar.start_loop()
    except:
        if args.log:
            logger.logger.exception('Exception on main handler')
            raise
        else:
            pass

if __name__ == "__main__":
    main()
