# Yet Another i3 Bar (yai3b)

Another i3 status bar write in Python

![preview](preview.png)

## How to use

See [Wiki](https://gitlab.com/naejel/yai3b/-/wikis/home)

## List of blocks

- Audio
- Battery
- Cpu Usage
- Custom Script
- Date
- Disk Usage
- Focused Window
- Music
- Network
- Text

## TODO

### Quality

- [ ] Documentation / Wiki
- [ ] PEP-8
- [ ] CI/CD
- [ ] Unit Tests
- [ ] Setuptools (setup.py + requirements.txt)

### Improvements

- [x] Themes Colors
- [x] Error Block
- [x] Block instanciation
- [x] Space around text in blocks
- [x] Separators instanciation
- [x] Argument parse
- [ ] Global Timeout for blocks
- [ ] CPU usage (multiple cast, same tests, yet resolved loop, ...)
- [x] Warning/ Critical
- [x] Override warning/crit back/foreground if back/foreground is set
- [x] Multiblocks
- [x] Open config file only one time
- [x] Try/Except for get_val and on_click instead of having all in blocks
- [ ] better launch_event method (factorize?)
- [ ] First event click
- [x] No extra space around multiblocks
- [ ] Custom icon for blocks
- [x] Multiple Same block in config
- [ ] Make over (custom click) opt by block not global

### util module

- [x] Rolling text
- [x] Launch Command
- [x] Get command
- [x] Time interval
- [ ] µs to string

### Block improvements

- [ ] Configurable criticity level
- [ ] Fixed width to prevent mooving bar

- Music

- [ ] Get album info
- [ ] Player volume Control
- [x] Rolling text for long string
- [x] Use Can... to not display not usable buttons
- [x] Dbus-send to read info instead of qdbus
- [x] Dedicated methods to use dbus-send

- Date

- [ ] Date Block (use locale day/month names, choose format)
- [ ] Format option

- Network

- [ ] Choose info to print (ip, type, domain, connection, dns, etc.)
- [ ] Rolling text
- [ ] Multiblock by interface
- [ ] Add other driver (ip, ifconfig)

- CPU

- [ ] Stat to get true average usage not since boot
- [ ] Load icon (low, mid, full)
- [ ] Criticity
- [ ] Rename time inertavl to measure time

- Disk usage

- [ ] Criticity
- [ ] Rename time inertavl to measure time

### New Blocks

- [x] Current Window
- [x] Network
- [x] Disk
- [x] CPU
- [ ] Temp
- [ ] Memory
- [ ] Keyboard
- [x] cmus / MediaPlayer2 protocol
- [ ] Xrandr
- [ ] GPU
